import React from 'react';
import './Card.css';
import TK from '../img/TK.png';
import S7 from '../img/S7.png';
import SU from '../img/SU.png';
import BA from '../img/BA.png';

export default class Card extends React.Component {
    static defaultProps = {
        origin: '',
        originName: '',
        destination: '',
        destinationName: '',
        departureDate: '',
        departureTime: '',
        arrivalDate: '',
        arrivalTime: '',
        carrier: '',
        stops: 0,
        price: 0,
    }
    render() {
        const {
            origin,
            originName,
            destination,
            destinationName,
            departureDate,
            departureTime,
            arrivalDate,
            arrivalTime,
            carrier,
            stops,
            price} = this.props;

        function convertDate(date){
           const dateArr = date.split('.');
           const enMonthArr = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
           const ruMonthArr = ['янв', 'фев', 'март', 'апр', 'май', 'июнь', 'июль', 'авг', 'сен', 'окт', 'нояб', 'дек'];
           const ruWeekArr = ['Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб', 'Вс'];
           const enMonth = enMonthArr[Number(dateArr[1])-1];
           const day = new Date(`${enMonth} ${Number(dateArr[0])}, 20${dateArr[2]} 12:00:00`);
           const dotw = day.getDay();

           return `${Number(dateArr[0])} ${ruMonthArr[Number(dateArr[1])-1]} 20${dateArr[2]}, ${ruWeekArr[dotw - 1]}`
        }

        let depDate = convertDate(departureDate);
        let arDate = convertDate(arrivalDate);

        let logo;

        switch (carrier){
            case "TK": logo = TK;
            break;
            case "S7": logo = S7;
            break;
            case "SU": logo = SU;
            break;
            case "BA": logo = BA;
            break;
            default:{
            }
        }

        let end;

        switch (stops){
            case 1: end = " пересадка";
            break;
            case 2:
            case 3:
            case 4: end = " пересадки";
            break;
            default:{
                end = " пересадок";
            }
        }

        return (
            <div className={'card'}>
                <div className={'card-left'}>
                    <div className={'card__logo-block'}>
                        <img alt={"логотип авиакомпании " + carrier} src={logo} className={'card__logo'}/>
                    </div>
                    <button className={'card-button'}>Купить<br/>за {price}</button>
                </div>
                <div className={"card-right"}>
                    <div className={"card-right__block"}>
                        <div className={"card-right__block-left"}>
                            <span className={"time"}>{departureTime}</span> <br/>
                            <span className={"places"}>{origin}, {originName}</span><br/>
                            <span className={"date"}>{depDate}</span>
                        </div>
                        <div className={"stops"}>
                            {stops} {end} <br/>
                            <div className={"horizontalLine"}/>
                            <span className={"horizontalLine-plane"}>✈</span>
                        </div>
                        <div className={"card-right__block-right"}>
                            <span className={"time"}>{arrivalTime}</span> <br/>
                            <span className={"places"}>{destination}, {destinationName}</span> <br/>
                            <span className={"date"}>{arDate}</span>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}