import React from 'react';
import Card from "../Card/Card";
import './CardList.css';
import '../Filter/Filter.css'

export default class CardList extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            All: true,
            everCheck: true,
            checks: []
        }
    }

    static defaultProps = {
        ticketsList: [],
        currency: '',
    }

    reRender = (value) => {
        if(value){
            this.currency = value;
        }
        this.forceUpdate();
    };

    stopsCheck = (value) => {
        let checkArr = this.checks || ['All'];
        let flag = false;
        checkArr.forEach( (element) => {
            if (value === element){
                flag = true;
                checkArr = checkArr.filter((n) => {return n !== element});
            }
        })
        if (!flag){
            checkArr.push(value);
        }
        this.checks = checkArr;
    };

    oneCheck = (id) => {
        if (id === 'All'){
            this.checks = ["All"];
        } else {
            this.checks = [Number.id];
        }
    }

    render() {

        function oneCheckClass(){
            const arrOneCheck = ['All', '0', '1', '2', '3'];
            for (let i = 0; i < arrOneCheck.length; i++){
                const checkbox = document.getElementById(arrOneCheck[i]);
                checkbox.checked = false
            }
        }

        let flag = false;

        let stops0 = false;
        let stops1 = false;
        let stops2 = false;
        let stops3 = false;

        let arrCheck = this.checks;

        if (arrCheck !== undefined){
            arrCheck.forEach((e) =>{
               if (e === "All"){
                   flag = true;
               }
                if (e === 0){
                   stops0 = true;
               }
               if (e === 1){
                   stops1 = true;
               }
               if (e === 2){
                   stops2 = true;
               }
               if (e === 3){
                   stops3 = true;
               }

            });
        }

        let currency = this.currency;

        function checkButton(e, value) {

            let rub = document.getElementById('RUB');
            let usd = document.getElementById('USD');
            let eur = document.getElementById('EUR');

            e.target.classList.remove('filter-currency__button');
            e.target.classList.add('filter-currency__button-click');

            switch (value){
                case "RUB": removeClass(usd);
                    removeClass(eur);
                    break;
                case "USD": removeClass(rub);
                    removeClass(eur);
                    break;
                case "EUR": removeClass(rub);
                    removeClass(usd);
                    break;
                default:{
                }
            }
        }

        function removeClass(e){
            e.classList.add('filter-currency__button');
            e.classList.remove('filter-currency__button-click');
        }

        let id = 0;
        return (
            <div className={'App-content'}>
                <div className={"filter"}>
                    <div className={"filter-currency"}>
                        <span className={"filter-chapter"}>ВАЛЮТА</span> <br/><br/>
                        <div>
                            <button className={"filter-currency__button-click"} id={"RUB"} value={"RUB"} onClick={(e) => {checkButton(e, "RUB"); this.reRender("RUB")}}>RUB</button>
                            <button className={"filter-currency__button"} id={"USD"} value={"USD"} onClick={(e) => {checkButton(e, "USD"); this.reRender("USD")}}>USD</button>
                            <button className={"filter-currency__button"} id={"EUR"} value={"EUR"} onClick={(e) => {checkButton(e, "EUR"); this.reRender("EUR")}}>EUR</button>
                        </div>
                    </div>
                    <div className={"filter-stops"}>
                        <span className={"filter-chapter chapter-stops"}>КОЛИЧЕСТВО ПЕРЕСАДОК</span> <br/><br/>
                        <div className={"checkbox-block"}>
                            <input type="checkbox" id="All" defaultChecked={!flag} onClick={() => {this.stopsCheck("All"); this.reRender(currency)}}/> <label htmlFor="All" className={"checkbox-label"}><span>Все</span><span onClick={() => {this.oneCheck("All"); oneCheckClass(); this.reRender(currency)}} className={"hidden-span first"}>ТОЛЬКО</span></label>
                        </div>

                        <div className={"checkbox-block"}>
                            <input type="checkbox" id="0"  onClick={() => {this.stopsCheck(0); this.reRender(currency)}}/> <label htmlFor="0" className={"checkbox-label"}><span>Без пересадок</span><span onClick={() => {this.oneCheck("0"); oneCheckClass(); this.reRender(currency)}} className={"hidden-span second"}>ТОЛЬКО</span></label>
                        </div>

                        <div className={"checkbox-block"}>
                            <input type="checkbox" id="1"  onClick={() => {this.stopsCheck(1); this.reRender(currency)}}/> <label htmlFor="1" className={"checkbox-label"}><span>1 пересадка</span><span onClick={() => {this.oneCheck("1"); oneCheckClass(); this.reRender(currency)}} className={"hidden-span"}>ТОЛЬКО</span></label>
                        </div>

                        <div className={"checkbox-block"}>
                            <input type="checkbox" id="2"  onClick={() => {this.stopsCheck(2); this.reRender(currency)}}/> <label htmlFor="2" className={"checkbox-label"}><span>2 пересадки</span><span onClick={() => {this.oneCheck("2"); oneCheckClass(); this.reRender(currency)}} className={"hidden-span"}>ТОЛЬКО</span></label>
                        </div>

                        <div className={"checkbox-block"}>
                            <input type="checkbox" id="3"  onClick={() => {this.stopsCheck(3); this.reRender(currency)}}/> <label htmlFor="3" className={"checkbox-label"}><span>3 пересадки</span><span onClick={() => {this.oneCheck("3"); oneCheckClass(); this.reRender(currency)}} className={"hidden-span"}>ТОЛЬКО</span></label>
                        </div>
                    </div>
                </div>
                <div className={'card-list'}>
                    {
                        this.props.ticketsList.map((ticket) => {
                            id++;
                            let calculatedPrice;
                            switch (currency){
                                case "RUB": calculatedPrice = `${new Intl.NumberFormat('ru-Ru').format(ticket.price)}₽`;
                                    break;
                                case "USD": calculatedPrice = `${new Intl.NumberFormat('ru-Ru').format(Math.round(ticket.price/50))}$`;
                                    break;
                                case "EUR": calculatedPrice = `${new Intl.NumberFormat('ru-Ru').format(Math.round(ticket.price/60))}€`;
                                    break;
                                default:{
                                    calculatedPrice = `${new Intl.NumberFormat('ru-Ru').format(ticket.price)}₽`
                                }
                            }
                            if (flag || arrCheck === undefined || arrCheck.length === 0) {
                                return (
                                    <div className={'card-list__item'} key={id}>
                                        <Card
                                            origin={ticket.origin}
                                            originName={ticket['origin_name']}
                                            destination={ticket.destination}
                                            destinationName={ticket['destination_name']}
                                            departureDate={ticket['departure_date']}
                                            departureTime={ticket['departure_time']}
                                            arrivalDate={ticket['arrival_date']}
                                            arrivalTime={ticket['arrival_time']}
                                            carrier={ticket.carrier}
                                            stops={ticket.stops}
                                            price={calculatedPrice}
                                        />
                                    </div>
                                )
                            } else {
                                if (stops0 && ticket.stops === 0){
                                    return (
                                        <div className={'card-list__item'} key={id}>
                                            <Card
                                                origin={ticket.origin}
                                                originName={ticket['origin_name']}
                                                destination={ticket.destination}
                                                destinationName={ticket['destination_name']}
                                                departureDate={ticket['departure_date']}
                                                departureTime={ticket['departure_time']}
                                                arrivalDate={ticket['arrival_date']}
                                                arrivalTime={ticket['arrival_time']}
                                                carrier={ticket.carrier}
                                                stops={ticket.stops}
                                                price={calculatedPrice}
                                            />
                                        </div>
                                    )
                                }
                                if (stops1 && ticket.stops === 1){
                                    return (
                                        <div className={'card-list__item'} key={id}>
                                            <Card
                                                origin={ticket.origin}
                                                originName={ticket['origin_name']}
                                                destination={ticket.destination}
                                                destinationName={ticket['destination_name']}
                                                departureDate={ticket['departure_date']}
                                                departureTime={ticket['departure_time']}
                                                arrivalDate={ticket['arrival_date']}
                                                arrivalTime={ticket['arrival_time']}
                                                carrier={ticket.carrier}
                                                stops={ticket.stops}
                                                price={calculatedPrice}
                                            />
                                        </div>
                                    )
                                }
                                if (stops2 && ticket.stops === 2){
                                    return (
                                        <div className={'card-list__item'} key={id}>
                                            <Card
                                                origin={ticket.origin}
                                                originName={ticket['origin_name']}
                                                destination={ticket.destination}
                                                destinationName={ticket['destination_name']}
                                                departureDate={ticket['departure_date']}
                                                departureTime={ticket['departure_time']}
                                                arrivalDate={ticket['arrival_date']}
                                                arrivalTime={ticket['arrival_time']}
                                                carrier={ticket.carrier}
                                                stops={ticket.stops}
                                                price={calculatedPrice}
                                            />
                                        </div>
                                    )
                                }
                                if (stops3 && ticket.stops === 3){
                                    return (
                                        <div className={'card-list__item'} key={id}>
                                            <Card
                                                origin={ticket.origin}
                                                originName={ticket['origin_name']}
                                                destination={ticket.destination}
                                                destinationName={ticket['destination_name']}
                                                departureDate={ticket['departure_date']}
                                                departureTime={ticket['departure_time']}
                                                arrivalDate={ticket['arrival_date']}
                                                arrivalTime={ticket['arrival_time']}
                                                carrier={ticket.carrier}
                                                stops={ticket.stops}
                                                price={calculatedPrice}
                                            />
                                        </div>
                                    )
                                }
                                return null;
                            }
                        })
                    }
                </div>
            </div>
        )
    }
}

