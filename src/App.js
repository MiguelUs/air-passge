import './App.css';
import CardList from "./components/CardList/CardList";
import React from "react";
import logo from './components/img/air_in_circle.png';

export default class App extends React.Component {


  constructor(props) {
    super(props);

    this.state = {
      tickets: [],
    }
  }

  componentDidMount() {
    const json = require('./tickets.json')
    this.setState({tickets: json.tickets});
  }

  render() {

    return (
        <div className="App">
          <div className={"logo-block"}>
            <img alt={"логотип"} className={"logo"} src={logo}/>
          </div>
            {this.renderTickets()}
        </div>
    );
  }
  renderTickets() {
    if (this.state.tickets.length) {
      return <CardList ticketsList={this.state.tickets}/>
    }
    return <div>Загрузка...</div>
  }
}

